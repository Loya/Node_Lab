const http = require('http');
const fs = require('fs');

//aquí no existen métodos, aquí todos son funciones//
//funcion anónima//

http.createServer(function(request, response){
  fs.readFile('./index.html', function(error, html){
    response.write(html);
    response.end();
  });

}).listen(3000);

/*let server = function(){

//};
//http.createServer();




//http.createServer(function(){

});*/
